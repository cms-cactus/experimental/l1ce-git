change sources
 - L1CE
 - Gitlab repo pushes
   - regular: feature branches
   - exceptional: push by cactus (disaster recovery related?)
 - NOT db. if changes made there -> ignored and overwritten at next valid change
   - periodic consistency checks? (admin panel)



local expert makes change to used mode key
user perspective: makes change in L1CE, and clicks request approval, L1Doc clicks approve and deploy
system perspective:
  -> expert opens L1CE -> at first change a branch is created (contains username)
  -> changes affect L1Doc territory -> L1CE offers to request approval of this change -> creates merge request
  -> L1Doc gets notified -> clicks approve and deploy
  -> L1CE approves the merge request as cactus and requests merge (with branch deletion)
     -> Gitlab CI performs flash to DB (which is a L1CE binary command for disaster recovery cases)
  -> periodic gitlab API check no longer returns 304 (periodic check for branches & last commits)
  -> L1CE pulls changes (override local conflicts)
     -> rebase all active branches to new master HEAD
     -> push changes

local expert makes change to used mode key in git
user perspective: checkout l1ce-git, make changes, create new branch & commit, creates merge request
system perspective:
  -> periodic gitlab API check no longer returns 304 (periodic check for merge request list)
  -> changes affect L1Doc territory -> L1Doc gets notified -> clicks approve and deploy
  -> L1CE performs merge

local expert makes change to unused mode key
user perspective: makes change in L1CE, and clicks save
system perspective:
  -> expert opens L1CE -> at first change a branch is created (contains username)
  -> changes only affect the experts' territory -> L1CE offers to save changes
  -> L1CE performs merge

local expert makes change to unused mode key in git
user perspective: checkout l1ce-git, make changes, create new branch & commit, creates merge request
  -> periodic gitlab API check no longer returns 304 (periodic check for merge request list)
  -> changes affect only experts' territory -> L1CE comments a url to L1CE where the expert can click deploy
  -> expert goes to L1CE and clicks deploy















--------------------------------------------------------------------------------------
local expert makes change to unused mode key
user perspective: makes changes in L1CE and clicks save
system perspective:
expert opens L1CE -> at first change a branch is created (contains username)
  -> changes only affect expert territory -> L1CE allows direct save
  -> expert clicked save
  -> merge

local export makes change to used mode key
user perspective: makes change in L1CE, and clicks request approval, L1Doc approves and merges
system perspective:
expert opens L1CE -> at first change a branch is created (contains username)
  -> changes affect L1Doc territory -> L1CE offers to request approval of this change
  -> everyone affected (=L1Doc) needs to approve the change -> L1Doc clicks approve
  -> merge

local expert makes change to unused mode key in git
user perspective: makes some changes and commits to a (new?) branch (master is protected)
system perspective:
  -> periodic gitlab API check no longer returns 304
  -> L1CE compares changed branches
  -> git pulls latest changes, override local conflicts
  -> force reload of affected users (only if expert pushed to a L1CE maintained user branch)
  -> 